 * Iniciar rust

#+begin_src emacs-lisp
(require 'rust-mode)
(add-hook 'rust-mode-hook 'cargo-minor-mode)
(setq rust-format-on-save t)
(add-hook 'rust-mode-hook
          (lambda () (prettify-symbols-mode)))
(define-key rust-mode-map (kbd "C-c C-c") 'rust-run)
(use-package tree-sitter
  :config
  (require 'tree-sitter-langs)
  (global-tree-sitter-mode)
  (add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode))
(add-hook 'rust-mode-hook #'lsp)
#+end_src

** Cargo minor mode

#+begin_src emacs-lisp
(add-hook 'rust-mode-hook 'cargo-minor-mode)
;; (use-package cargo-mode
;;   :config
;;   (add-hook 'rust-mode-hook 'cargo-minor-mode))

(setq cargo-process--command-bench "bench")
(setq cargo-process--command-build "build")
(setq cargo-process--command-clean "clean")
(setq cargo-process--command-doc "doc")
(setq cargo-process--command-doc-open "doc --open")
(setq cargo-process--command-new "new")
(setq cargo-process--command-init "init")
(setq cargo-process--command-run "run")
(setq cargo-process--command-run-bin "run --bin")
(setq cargo-process--command-run-example "run --example")
(setq cargo-process--command-search "search")
(setq cargo-process--command-test "test")
(setq cargo-process--command-current-test "test")
(setq cargo-process--command-current-file-tests "test")
(setq cargo-process--command-update "update")
(setq cargo-process--command-fmt "fmt")
(setq cargo-process--command-check "check")
(setq cargo-process--command-clippy "clippy")
(setq cargo-process--command-add "add")
(setq cargo-process--command-rm "rm")
(setq cargo-process--command-upgrade "upgrade")
(setq cargo-process--command-audit "audit -f")
(setq cargo-process--command-script "script")
(setq cargo-process--command-watch "watch -x build")
#+end_src

#+RESULTS:
: watch -x build


Previamente instalar.

#+begin_src shell
rustup component add rustfmt-preview
cargo install cargo-check
cargo install clippy
cargo install cargo-script
cargo install cargo-edit
cargo install cargo-audit
cargo install cargo-watch
#+end_src


* Definir función que llame a rustic

#+begin_src emacs-lisp
;; (defun rk/rustic-mode-hook ()
;;   ;; so that run C-c C-c C-r works without having to confirm, but don't try to
;;   ;; save rust buffers that are not file visiting. Once
;;   ;; https://github.com/brotzeit/rustic/issues/253 has been resolved this should
;;   ;; no longer be necessary.
;;   (when buffer-file-name
;;     (setq-local buffer-save-without-query t)))
#+end_src

#+RESULTS:
: rk/rustic-mode-hook

* Habilitación de rustic

Habilitar las bases del módulo.

Cargar paquete de directorio descargado


#+RESULTS:
  
(use-package rustic
;;   :ensure
;;   :bind (:map rustic-mode-map
;; 			  ("<f6>" . rustic-format-buffer)
;; 			  ("M-j" . lsp-ui-imenu)
;; 			  ("M-?" . lsp-find-references)
;; 			  ("C-c C-c l" . flycheck-list-errors)
;; 			  ("C-c C-c a" . lsp-execute-code-action)
;; 			  ("C-c C-c r" . lsp-rename)
;; 			  ("C-c C-c q" . lsp-workspace-restart)
;; 			  ("C-c C-c Q" . lsp-workspace-shutdown)
;; 			  ("C-c C-c s" . lsp-rust-analyzer-status)
;; 			  ("C-c C-c e" . lsp-rust-analyzer-expand-macro)
;; 			  ("C-c C-c d" . dap-hydra)
;; 			  ("C-c C-c h" . lsp-ui-doc-glance)
;; 			  ("C-c C-c +" . rustic-cargo-add)
;; 			  ("C-c C-c -" . rustic-cargo-rm);; existe detelet?
;; 			  ("C-c C-c *" . rustic-cargo-upgrade);; existe detelet?
;; 			  ("C-c C-c d" . rustic-cargo-doc))			  
;;   :config
;;   (require 'lsp-rust)
;;   (setq lsp-rust-analizer-completion-add-call-parenthesis nil)
;;   (setq lsp-eldoc-hook nil)
;;   (setq lsp-enable-symbol-highlighting nil)
;;   (setq lsp-signature-auto-activate nil)
;;   ;; comment to disable rustfmt on save
;;   (setq rustic-format-on-save t)
;;   (setq rustic-lsp-client nil)
;;   (setq rustic-lsp-server 'rls)
;;   (setq rustic-analyzer-command '("~/.cargo/bin/rust-analyzer"))
;;   (add-hook 'rustic-mode-hook 'rk/rustic-mode-hook)
;;  )
;; (message "load rustic-mode ok
")
  #+end_src

  #+RESULTS:
  : load rustic-mode ok


** Format on save

   Al guardar se revisa y formatea bajo estándar.
   
   #+begin_src emacs-lisp
  ;; (setq rustic-format-on-save t)
  ;; (add-hook 'rustic-mode-hook 'rk/rustic-mode-hook)
   #+end_src

   #+RESULTS:
   | rk/rustic-mode-hook |



** Activación de lsp con rust

Llamamos al paquete lsp y le activamos la parte *rust*

#+begin_src emacs-lisp
(use-package lsp-mode
  :ensure
  :commands lsp
  :custom
  ;; what to use when checking on-save. "check" is default, I prefer clippy
  (lsp-rust-analyzer-cargo-watch-command "clippy")
  (lsp-eldoc-render-all t)
  (lsp-idle-delay 0.6)
  (lsp-rust-analyzer-server-display-inlay-hints t)
  :config
  (add-hook 'lsp-mode-hook 'lsp-ui-mode))
#+end_src

#+RESULTS:
: t

También, activamos la *ui* para el *lsp*

#+begin_src emacs-lisp
(use-package lsp-ui
  :ensure
  :commands lsp-ui-mode
  :custom
  (lsp-ui-peek-always-show t)
  (lsp-ui-sideline-show-hover t)
  (lsp-ui-doc-enable nil))
#+end_src

#+RESULTS:


** Activación yasnippet

Provee snippets para rust.

#+begin_src emacs-lisp
(use-package yasnippet
  :ensure
  :config
  (yas-reload-all)
  (add-hook 'prog-mode-hook 'yas-minor-mode)
  (add-hook 'text-mode-hook 'yas-minor-mode))
#+end_src

#+RESULTS:
: t


** Asociar con company

Mostrar el menú de opciones

#+begin_src emacs-lisp
(use-package company
  :ensure
  :bind
  (:map company-active-map
              ("C-n". company-select-next)
              ("C-p". company-select-previous)
              ("M-<". company-select-first)
              ("M->". company-select-last))
  (:map company-mode-map
        ("<tab>". tab-indent-or-complete)
        ("TAB". tab-indent-or-complete)))
#+end_src

#+RESULTS:
: tab-indent-or-complete

** Company con yasnippet
   
Funciones que asocian ambos módulos

#+begin_src emacs-lisp
(defun company-yasnippet-or-completion ()
  (interactive)
  (or (do-yas-expand)
      (company-complete-common)))

(defun check-expansion ()
  (save-excursion
    (if (looking-at "\\_>") t
      (backward-char 1)
      (if (looking-at "\\.") t
        (backward-char 1)
        (if (looking-at "::") t nil)))))

(defun do-yas-expand ()
  (let ((yas/fallback-behavior 'return-nil))
    (yas/expand)))

(defun tab-indent-or-complete ()
  (interactive)
  (if (minibufferp)
      (minibuffer-complete)
    (if (or (not yas/minor-mode)
            (null (do-yas-expand)))
        (if (check-expansion)
            (company-complete-common)
          (indent-for-tab-command)))))
#+end_src

#+RESULTS:
: tab-indent-or-complete

** Ubicar el cargo-toml

#+begin_src emacs-lisp
 #+end_src

** Habilitar debugging

Se usa *dap* para hacer debugging.

#+begin_src emacs-lisp
(use-package exec-path-from-shell
  :ensure
  :init (exec-path-from-shell-initialize))

(when (executable-find "lldb-mi")
  (use-package dap-mode
    :ensure
    :config
    (dap-ui-mode)
    (dap-ui-controls-mode 1)

    (require 'dap-lldb)
    (require 'dap-gdb-lldb)
    ;; installs .extension/vscode
    (dap-gdb-lldb-setup)
    (dap-register-debug-template
     "Rust::LLDB Run Configuration"
     (list :type "lldb"
           :request "launch"
           :name "LLDB::Run"
	   :gdbpath "rust-lldb"
           ;; uncomment if lldb-mi is not in PATH
           ;; :lldbmipath "path/to/lldb-mi"
           ))))
#+end_src

#+RESULTS:
   
