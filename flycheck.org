Para configurar flycheck de manera general, más allá del lenguaje
particualar

* Flycheck 


Flycheck es el módulo principal que hace la revisiona activa del
codigo.

Inicializa además con modo automático de /flycheck/.

#+begin_src emacs-lisp
(use-package flycheck
:ensure t
:init (global-flycheck-mode))
#+end_src

* Enable flycheck with mypy/pylint

Encadenar los /checkers/ para flycheck.

#+begin_src emacs-lisp
(flycheck-add-next-checker 'python-flake8 'python-pylint 'python-mypy)
#+end_src



