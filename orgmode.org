

* Habilitar babel para interpretar trozos de código

Para que los documentos org-mode puedan contener trozos de código que sean ejecutables, habilitar lo siguiente, agregando 
los lenguajes en la lista.

#+begin_src emacs-lisp
(org-babel-do-load-languages
 'org-babel-load-languages
 (append org-babel-load-languages
	 '(
   (sed . t)
   (awk . t)
   (calc .t)
   (C . t)
   (haskell . t)
   (latex . t)
   (js . t)
   (gnuplot . t)
   (emacs-lisp . t)
   (haskell . t)
   (perl . t)
   (python . t)
   (plantuml . t)
   (lua . t)
   (org . t)
   (shell . t)
   ;; org-babel does not currently support php.  That is really sad.
   ;;(php . t)
   (R . t)
   (scheme . t)
   (sql . t)
   (shell . t)
   (http . t)
   (verb . t)
   (dot . t)
   (ditaa . t)
   ;;(rust . t)
   (plantuml . t)
   ;;(sqlite . t)
   )))
#+end_src

#+RESULTS:


Asimismo, para evitar que pregunte por una nueva ejecución cada vez.

#+begin_src emacs-lisp
(defun my-org-confirm-babel-evaluate (lang body)
	    (not (member lang '(
				"bash"
				"shell"
				"python"
				"C"
				"awk"
				"sql"
				"lua"
				"gnuplot"
				"haskell"
				"restclient"
				"verb"
				"emacs-lisp"
				"dot"
				"ditaa"
				"plantuml"
				"dot"
				;;"rust"
				)
			 )
		 )
	    )  ; don't ask for ditaa
(setq org-confirm-babel-evaluate 'my-org-confirm-babel-evaluate)
#+end_src

#+RESULTS:
: my-org-confirm-babel-evaluate

* Plantutml

#+begin_src emacs-lisp
(setq org-plantuml-jar-path (expand-file-name "/opt/plantuml/plantuml.jar"))
#+end_src

#+RESULTS:
: /opt/plantuml/plantuml.jar


Helpers

#+begin_src emacs-lisp
(require 'iimage)
(autoload 'iimage-mode "iimage" "Support Inline image minor mode." t)
(autoload 'turn-on-iimage-mode "iimage" "Turn on Inline image minor mode." t)
(add-to-list 'iimage-mode-image-regex-alist '("@startuml\s+\\(.+\\)" . 1))

;; Rendering plantuml
(defun plantuml-render-buffer ()
  (interactive)
  (message "PLANTUML Start rendering")
  (shell-command (concat "java -jar  /opt/plantuml/plantuml.jar" 
                         buffer-file-name))
  (message (concat "PLANTUML Rendered:  " (buffer-name))))

;; Image reloading
(defun reload-image-at-point ()
  (interactive)
  (message "reloading image at point in the current buffer...")
  (image-refresh (get-text-property (point) 'display)))

;; Image resizing and reloading
(defun resize-image-at-point ()
  (interactive)
  (message "resizing image at point in the current buffer123...")
  (let* ((image-spec (get-text-property (point) 'display))
         (file (cadr (member :file image-spec))))
    (message (concat "resizing image..." file))
    (shell-command (format "convert -resize %d %s %s " 
                           (* (window-width (selected-window)) (frame-char-width))
                           file file))
    (reload-image-at-point)))

#+end_src

#+RESULTS:
: resize-image-at-point

* Habilitar extensiones de imágenes

Para permitir visualizar imágenes con diferentes extensiones o compresiones.


#+begin_src emacs-lisp
(setq image-file-name-extensions
   (quote
    (
     "png"
     "jpeg"
     "jpg"
     "gif"
     "tiff"
     "tif"
     "xbm"
     "xpm"
     "pbm"
     "pgm"
     "ppm"
     "pnm"
     "svg"
     "pdf"
     "bmp")))
#+end_src


* Exportaciones de documentos.

#+begin_src emacs-lisp
(electric-indent-mode 0)
(require 'ox-latex)
(setq org-reveal-root "../")
(add-to-list 'org-latex-packages-alist '("" "minted"))
(setq org-latex-listings 'minted)
(setq org-image-actual-width nil)
(setq org-latex-create-formula-image-program 'dvisvgm)
(setq-default auto-fill-function 'do-auto-fill)
(setq org-src-preserve-indentation t)

#+end_src


Selección de intérprete latex.

#+begin_src emacs-lisp
(setq org-latex-pdf-process
        (let
            ((cmd (concat "lualatex -shell-escape -interaction nonstopmode"
                          " --synctex=1"
                          " -output-directory %o %f")))
          (list cmd
                "cd %o; if test -r %b.idx; then makeindex %b.idx; fi"
                "cd %o; bibtex %b"
                cmd
                cmd)))
  
  ;; Sample minted options.
  (setq org-latex-minted-options '(
                                   ("frame" "lines")
                                   ("fontsize" "\\scriptsize")
                                   ("xleftmargin" "\\parindent")
                                   ("linenos" "")
                                   ))
  (setq org-src-fontify-natively t)
  (add-hook 'prog-mode-hook #'auto-fill-mode)
  (add-hook 'text-mode-hook #'auto-fill-mode)
#+end_src


* Exportar con sintaxis highlight

  Será necesario instalar *pygments* previamente.

  #+begin_src emacs-lisp
;; (setq org-latex-minted-options '(
;;                                  ("frame" "lines")
;;                                  ("fontsize" "\\scriptsize")
;;                                  ("xleftmargin" "\\parindent")
;;                                  ("linenos" "")
;;                                  ))
;; (setq org-src-fontify-natively t)
;; (add-hook 'prog-mode-hook #'auto-fill-mode)
;; (add-hook 'text-mode-hook #'auto-fill-mode)
  #+end_src

* Tabs controlados

#+begin_src emacs-lisp
;; tab settings
(setq-default org-src-tab-acts-natively t)
(setq-default org-src-preserve-indentation t)
(setq-default org-edit-src-content-indentation 2)
(setq-default org-adapt-indentation nil)
(setq-default electric-indent-mode nil)
(setq default-tab-width 4)
(setq indent-tabs-mode nil)
#+end_src
