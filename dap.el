(use-package dap-mode)

(setq dap-auto-configure-features 
	  '(sessions locals controls tooltip))

(require 'dap-python)

(dap-register-debug-template "Rust::GDB Run Configuration"
                             (list :type "gdb"
                                   :request "launch"
                                   :name "GDB::Run"
                           :gdbpath "rust-gdb"
                                   :target nil
                                   :cwd nil))
