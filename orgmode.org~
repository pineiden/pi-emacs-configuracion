

* Habilitar babel para interpretar trozos de código

Para que los documentos org-mode puedan contener trozos de código que sean ejecutables, habilitar lo siguiente, agregando 
los lenguajes en la lista.

#+begin_src emacs-lisp
(org-babel-do-load-languages
 'org-babel-load-languages
 (append org-babel-load-languages
	 '(
   (sed . t)
   (awk . t)
   (calc .t)
   (C . t)
   (haskell . t)
   (latex . t)
   (js . t)
   (gnuplot . t)
   (emacs-lisp . t)
   (haskell . t)
   (perl . t)
   (python . t)
   (plantuml . t)
   (lua . t)
   (org . t)
   (shell . t)
   ;; org-babel does not currently support php.  That is really sad.
   ;;(php . t)
   (R . t)
   (scheme . t)
   (sql . t)
   (shell . t)
   (http . t)
   (verb . t)
   (dot . t)
   (ditaa . t)
   ;;(rust . t)
   ;;(sqlite . t)
   )))
#+end_src


Asimismo, para evitar que pregunte por una nueva ejecución cada vez.

#+begin_src emacs-lisp
(defun my-org-confirm-babel-evaluate (lang body)
	    (not (member lang '(
				"bash"
				"shell"
				"python"
				"C"
				"awk"
				"sql"
				"lua"
				"gnuplot"
				"haskell"
				"restclient"
				"verb"
				"emacs-lisp"
				"dot"
				"ditaa"
				"plantuml"
				;;"rust"
				)
			 )
		 )
	    )  ; don't ask for ditaa
(setq org-confirm-babel-evaluate 'my-org-confirm-babel-evaluate)
#+end_src

* Habilitar extensiones de imágenes

Para permitir visualizar imágenes con diferentes extensiones o compresiones.


#+begin_src emacs-lisp
(setq image-file-name-extensions
   (quote
    (
     "png"
     "jpeg"
     "jpg"
     "gif"
     "tiff"
     "tif"
     "xbm"
     "xpm"
     "pbm"
     "pgm"
     "ppm"
     "pnm"
     "svg"
     "pdf"
     "bmp")))
#+end_src


* Exportaciones de documentos.

#+begin_src emacs-lisp
(electric-indent-mode 0)
(require 'ox-latex)
(setq org-reveal-root "../")
(add-to-list 'org-latex-packages-alist '("" "minted"))
(setq org-latex-listings 'minted)
(setq org-image-actual-width nil)
(setq org-latex-create-formula-image-program 'dvisvgm)
(setq-default auto-fill-function 'do-auto-fill)
(setq org-src-preserve-indentation t)

#+end_src


Selección de intérprete latex.

#+begin_src emacs-lisp
(setq org-latex-pdf-process
        (let
            ((cmd (concat "lualatex -shell-escape -interaction nonstopmode"
                          " --synctex=1"
                          " -output-directory %o %f")))
          (list cmd
                "cd %o; if test -r %b.idx; then makeindex %b.idx; fi"
                "cd %o; bibtex %b"
                cmd
                cmd)))
  
  ;; Sample minted options.
  (setq org-latex-minted-options '(
                                   ("frame" "lines")
                                   ("fontsize" "\\scriptsize")
                                   ("xleftmargin" "\\parindent")
                                   ("linenos" "")
                                   ))
  (setq org-src-fontify-natively t)
  (add-hook 'prog-mode-hook #'auto-fill-mode)
  (add-hook 'text-mode-hook #'auto-fill-mode)
#+end_src


* Exportar con sintaxis highlight

  Será necesario instalar *pygments* previamente.

  #+begin_src emacs-lisp
(setq org-latex-minted-options '(
                                 ("frame" "lines")
                                 ("fontsize" "\\scriptsize")
                                 ("xleftmargin" "\\parindent")
                                 ("linenos" "")
                                 ))
(setq org-src-fontify-natively t)
(add-hook 'prog-mode-hook #'auto-fill-mode)
(add-hook 'text-mode-hook #'auto-fill-mode)
  #+end_src

* Tabs controlados

#+begin_src emacs-lisp
;; tab settings
(setq-default org-src-tab-acts-natively t)
(setq-default org-src-preserve-indentation t)
(setq-default org-edit-src-content-indentation 2)
(setq-default org-adapt-indentation nil)
(setq-default electric-indent-mode nil)
(setq default-tab-width 4)
(setq indent-tabs-mode nil)
#+end_src
