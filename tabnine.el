(setq lsp-enable-links nil)
(use-package company-tabnine :ensure t)
(add-to-list 'company-backends #'company-tabnine)

;; Trigger completion immediately.
(setq company-idle-delay 0)

;; Number the candidates (use M-1com, M-2 etc to select completions).
(setq company-show-numbers t)
