% Created 2022-06-03 vie 12:30
% Intended LaTeX compiler: pdflatex
\documentclass[legalpaper,9pt,twoside,twocolumn,margin=.5in]{article}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\usepackage{geometry}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{upgreek}
\geometry{margin=1.0cm, top=1.0cm, headsep=1.0cm, footskip=1.0cm}
\newcommand{\R}{\ensuremath{\mathbb{R}}}
\author{David Pineda Osorio}
\date{\today}
\title{Control 2 Estadística}
\hypersetup{
 pdfauthor={David Pineda Osorio},
 pdftitle={Control 2 Estadística},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.5.3)}, 
 pdflang={Spanish}}
\begin{document}

\maketitle

\section{P1 Test de hip�tesis}
\label{sec:org7318f20}

Se tiene un \(X \sim \mathcal{N}(\mu,\sigma^2)\), sobre caracter�stica de
un colectivo de individuos, \(\mu\) es desconocida, \(\sigma=16\)
conocida. 

Dado el siguiente test de hip�tesis.

\begin{equation}
H_0: \mu = 100 \quad H_1: \mu > 100 
\end{equation}
Considerando \(X=(X_1,...,X_n)\) un muestreo aleatorio simple (MAS), y
un nivel de significancia \(\alpha \in (0,1)\).

\subsection{Realizar Z-test}
\label{sec:orgc35ab46}

Encontrar la regi�n cr�tica asociada. Suponiendo \(H_0\) verdadera, se
define un pivote \(Z=Z(X_1,..,X_n)\).

\begin{equation}
Z = \frac {\overline{X}-\mu_0} {\sigma/\sqrt{n}} = \frac{\overline{X}-100} {16/\sqrt{n}} \sim \mathcal{N}(0, 1)
\end{equation}

En que se tiene, además. 

\begin{equation}
\overline{X} = \frac {\sum{X_i}} {n} 
\end{equation}

Entonces, la probabilidad de rechazar para el nivel de significancia \(\alpha\).

\begin{equation}
P(Z \in R_z) = P(Z >= Z_0) = 1 - P(Z <= Z_0) = \alpha
\end{equation}

\subsection{Encontrar la región para valores}
\label{sec:orgdbbde07}

Siendo \(n=16\) y \(\alpha=0.05\) ser� posible encontrar la regi�n
cr�tica.

El valor para \(Z_0\) se puede encontrar por.

\begin{equation}
\frac {\overline{X}-100} {16/\sqrt{16}} = \frac {\overline{X} - 100}{4} <= 1.6449
\end{equation} 

Es decir, encontramos que el valor medio de X debe cumplir.

\begin{equation}
\overline{X} = 116.5794
\end{equation}

Se rechaza \(H_0\) si la media muestral supera ese valor.

Gr�ficamente tendremos.

\begin{figure}[htbp]
\centering
\includegraphics[width=280px]{./img/gauss_normal.png}
\caption{\label{fig:org3e4ef39}Zona de rechazo \(\alpha\)=0.05}
\end{figure}

\subsection{Incluir caso \(\mu\) es 110}
\label{sec:orgfa8a655}

Se cumple \(H_1\), se rechaza hip�tesis nula, las regiones relacionadas
con los diferentes tipos de error.

Se tiene la probabilidad.

\begin{equation}
P(se rechaza H_0 | H_1 cierta) = P(Z > Z_0) 
\end{equation}

Se cumple la hip�tesis \(H_1\) para \(\overline{X}>=116.579\).

\begin{description}
\item[{Error tipo I}] se rechaza \(H_0\) siendo cierta, o \textbf{nivel de
significancia para el test}
\end{description}

\begin{equation}
P(\overline{X}>100/ \mu=110) = P(Z  > \frac {116.5794 - 110} {4}) = 1 - \phi(1.64485) = \simeq = 0.05
\end{equation}

Se tiene una baja probabilidad de que se cometa error de tipo I. Esto
es porque para ese valor de \(\mu\) se encuentra cercano al valor de
hip�tesis nula.

\begin{description}
\item[{Error tipo II}] se acepta \(H_0\) siendo cierta \(H_1\)
\end{description}
Es decir \(Z<Z_0\) siendo que \(\mu=110\) (deber�a rechazarse)

La distribuci�n de \(H_1\) no es \(\mathcal{N}(0,1)\), por lo que se hace
un cambio de variable.

\begin{equation}
\frac {\overline{X}-\mu_0} {\sigma/\sqrt(n)} = \frac {\overline{X}- \mu + \mu - \mu_0} {\sigma/\sqrt(n)} = \frac {\overline{X}- \mu} {\sigma/\sqrt(n)}+ \frac {\mu - \mu_0} {\sigma/\sqrt(n)}
\end{equation}

Tendremos entonces.

\begin{equation}
\begin{split}
\beta(\mu) &= P(\overline{X}=100/ \mu=110) \\
&=P(Z <= \frac {116.5794 - 100} {4}-\frac {\mu - \mu_0} {\sigma/\sqrt(n))} = \phi(1.64485-2.5) = 0.196
\end{split}
\end{equation}

Existe una probabilidad de un 19.6\% que se cometa un error de tipo II.

Caso \(\mu \simeq 100\) y \(\mu >> 100\)

Es decir, para casos cercanos a \(\mu\) ser�  m�s probable cometer este
tipo de error, en cambio para valores alejados de \(H_0\) la tendencia se revierte.

\begin{figure}[htbp]
\centering
\includegraphics[width=250px]{./img/gauss_normal_rechazo_errores.png}
\caption{\label{fig:orgb28ea3d}Zona de rechazo, errores tipo I y II}
\end{figure}

\subsection{Potencia del test}
\label{sec:org1007955}

El an�lisis de potencia se deriva de la definici�n.

\begin{equation}
\pi(\theta) = P(rechazo H_0|H_0 falsa) = 1 - \beta(\mu) = .804
\end{equation}

Define el nivel de significancia m�s peque�o con que se rechaza \(H_0\).

Dado que p-valor es mayor a \(\alpha\) se acepta \(H_0\) con nivel de
significancia \(\alpha\).

\begin{figure}[htbp]
\centering
\includegraphics[width=250px]{./img/potencia_n_var.png}
\caption{\label{fig:orgf308a8d}Potencia, variando mu, diversos n}
\end{figure}
Asi como tambien, para diferentes \(\mu\) fijos, n variable se tiene que
la potencia mejora cuando \(\mu\) aumenta, converge para n grande a 0
o 1, esto segun el lado de \(H_0\) queda \(\mu\)

\begin{figure}[htbp]
\centering
\includegraphics[width=250px]{./img/potencia_mu_var.png}
\caption{\label{fig:orga13c7d4}Potencia, variando n, diversos mu}
\end{figure}
\subsection{Cambiando alpha}
\label{sec:orga8c2ea7}

De manera analoga, la potencia presenta comportamientos similares para
valores de  \(\mu\) en torno a 100, con leves desplazamientos \ref{fig:orgaf936e5}.

\begin{figure}[htbp]
\centering
\includegraphics[width=250px]{./img/potencia_alpha_var.png}
\caption{\label{fig:orgaf936e5}mu vs Potencia, distintos alpha}
\end{figure}
A mayor \(\alpha\) aumenta la posibilidad de cometer errores de tipo I,
dismuyendo en proporci�n los de tipo II. Lo que se refleja en un
aumento de la potencia.
\end{document}