import sys
import asyncio
from typing import Dict
from data import Station
from multi_station_layout_widget import MultiStationLayoutWidget

import pyqtgraph as pg
import functools
import concurrent.futures
import multiprocessing as mp
from multiprocessing import Manager, Queue

from PySide6.QtWidgets import (
    QApplication,
    QLabel,
    QMainWindow,
    QPushButton,
    QVBoxLayout,
    QWidget,
)
from PySide6.QtCore import (
    QObject, 
    QRunnable, 
    QThreadPool, 
    QTimer,
    Signal, 
    Slot
)
from qasync import QEventLoop, QThreadExecutor

# test imports
from datetime import datetime
from station_layout_widget import StationLayoutWidget
from station_ring_buffer import RingBuffer as RingBufferTest 
from utils import TimeAxisItem, timestamp
from functools import partial
from plot_station import PlotStation
import random

AEventLoop = type(asyncio.get_event_loop())



class QEventLoopPlus(QEventLoop, AEventLoop):
    pass

def create_chart(pg, win):
    # Enable antialiasing for prettier plots
    pg.setConfigOptions(antialias=True)
    p1 = win.addPlot(title="N",axisItems={'bottom': TimeAxisItem(orientation='bottom')})
    p2 = win.addPlot(title="E",axisItems={'bottom': TimeAxisItem(orientation='bottom')})
    p3 = win.addPlot(title="U",axisItems={'bottom': TimeAxisItem(orientation='bottom')})    
    #p1.setYRange(-110, 100)
    #p3.setYRange(-3, 3)
    #p2.setYRange(-3, 3)
    seconds = 30
    plot_station = PlotStation(seconds,p1,p2,p3)
    plot_station.enableAutoRange('xy', True)  ## stop auto-scaling after the first data set is plotted
    return plot_station


def create_ring_buffer(seconds):
    data = dict(
        seconds=seconds,
        name="test",
        time_field="dt_gen",
        ringbuffer=[],
        keys=["N","E","U"]
    )
    rb = RingBufferTest(**data)
    return rb

def update(plot_station, ring_buffer):
    """
    Axis and data, are the name and the new data from ringbuffer
    """
    # update take from ring buffer
    data = ring_buffer.update()
    # iterate over data
    #put over the plot
    now = datetime.utcnow()
    plot_station.update_x_axis(now)
    plot_station.setData(data)    
    


class MainWindow(QMainWindow):
    groups:Dict[str, MultiStationLayoutWidget]
    stations:Dict[str, Station]

    def __init__(self, groups=[], stations={}):
        super().__init__()
        if not groups:
            groups.append("MAIN")
        self.stations = stations
        parent_layout = QVBoxLayout()
        w = QWidget()
        win = StationLayoutWidget(size=(500,100))
        self.create_chart(win)
        #win.start(60)
        print("WIn",win)
        #self.main_win = self.create_charts(w, groups)
        self.main_win = win
        parent_layout.addWidget(self.main_win)
        w.setLayout(parent_layout)
        self.setCentralWidget(w)
        # the timer could activate on the active window?
        self.set_timer(self.main_win)
        self.show()

    def create_chart(self, win):
        plot_station = win.create_chart()#create_chart(pg, win)
        ring_buffer = create_ring_buffer(plot_station.seconds)
        # connect ring buffer with chart
        win.set_update(partial(update,plot_station,ring_buffer))
        self.set_timer(win)

    def create_charts(self, parent, groups):
        """
        TODO: Check if parent is for all...
        """
        print("Creating charts")
        for g in groups:
            print(g,self.stations)
            stations_group = {code:station for code, station in
                              self.stations.items() if station.group==g}
            win = MultiStationLayoutWidget(size=(500,100)).start(
                g,
                stations_group)
            print(win)
            self.groups[g] = win
        main_win = self.groups[groups[0]]
        pg.setConfigOptions(antialias=True)
        return main_win

   
    def set_timer(self, win):
        self.timer= QTimer(self)
        self.timer.timeout.connect(win.get_frame)
        self.timer.start(100)


def run_gui():
    stations = {
        "BASE": Station("BASE", "MAIN", 0)
    }
    # DBusQtMainLoop(set_as_default=True)
    print("Init RUNGUI")
    app = QApplication(sys.argv)
    loop = QEventLoopPlus(app)
    asyncio.set_event_loop(loop)
    gui = MainWindow(stations=stations)
    print("GUI->", gui)
    #gui.showMaximized()
    sys.exit(app.exec())


if __name__ == "__main__":
    print("Running GUI")
    workers = 2
    with concurrent.futures.ProcessPoolExecutor(
            workers) as executor:
        tasks = []
        loop = asyncio.get_event_loop()
        manager = Manager()
        task = loop.run_in_executor(
            executor,
            run_gui)
        tasks.append(task)

        try:
            loop.run_forever()
        except Exception as e:
            raise e
        except KeyBoardInterrupt:
            raise KeyboardInterruptError()
