% Created 2022-06-01 mi� 12:55
% Intended LaTeX compiler: pdflatex
\documentclass[legalpaper,9pt,twoside,twocolumn,margin=.3in]{article}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\usepackage[spanish]{babel}
\usepackage{geometry}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{upgreek}
\geometry{margin=1.0cm, top=1.0cm, headsep=1.0cm, footskip=1.0cm}
\newcommand{\R}{\ensuremath{\mathbb{R}}}
\author{David Pineda Osorio}
\date{\today}
\title{Aprendizaje de Máquinas: Tarea 2}
\hypersetup{
 pdfauthor={David Pineda Osorio},
 pdftitle={Aprendizaje de Máquinas: Tarea 2},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.5.3)}, 
 pdflang={Spanish}}
\begin{document}

\maketitle

\section{P1. Clasificaci�n, verosimilitud y entrop�a}
\label{sec:org21df2a6}



\subsection{Introducción}
\label{sec:orge61d205}

En el estudio de herramientas para la clasificación se pueden
encontrar dos enfoques, que se demostrará en esta sección, llegan a
a la misma solución dado un problema.

Dado un conjunto de observaciones \(\{(x_i,t_i)\}_{i=1}^N\) que
consisten en duplas instancia \(x  \in \mathbb{R}^d\), cada x presenta d
características e y un valor 'target' (\(t \in {1,...M}\)) que, en
conjunto  definen M clases diferentes.

La verosimilitud para el clasificador multiclase se define por.

\begin{equation}
\begin{split}
L =  \prod_{i=1}^{N} \prod_{j=1}^{M}  P(C_j | x_i)^{1_{t_{i=j}}} 
\end{split}
\end{equation}


\subsection{Igualdad de la log-verosimilitud y entropía cruzada.}
\label{sec:org70a3558}

Teniendo que la distribución empírica \(Q\) de una serie de datos, y las
distribuciones estimadas por el clasificador \(P\). Demostrar que la
\emph{log-verosimilitud negativa} coincide con la entropía cruzada.


\begin{equation}
\begin{split}
H_i(Q,P) = - \sum_{j=1}^{M} Q(C_j | x_i) log( P(C_j|x_i))
\end{split}
\end{equation}

En primera instancia, consideramos como una distribución empírica a
\(Q(C_{j}|x_i)=1_{t_{i=j}}\), es decir si se hace un recorrido sobre los
elementos de \(x_{i}=(x_1,...,x_d)\), Q es 1 solo cuando \(t_j=i\).

\begin{equation}
\begin{split}
L =  \prod_{i=1}^{N} \prod_{j=1}^{M}  P(C_j | x_i)^{Q(C_j |x_i)} 
\end{split}
\end{equation}

Aplicando \emph{log} para encontrar la log-verosimilitud.

\begin{equation}
\begin{split}
\lambda(L) =  \sum_{i=1}^{N} \sum_{j=1}^{M}  Q(C_j |x_i) log \{ P(C_j | x_i) \}
\end{split}
\end{equation}

Por lo tanto, tenemos que.

\begin{equation}
\begin{split}
\lambda(L)_i = \sum_{j=1}^{M}  Q(C_j |x_i) log \{ P(C_j | x_i) \}
\end{split}
\end{equation}

Entonces, la relación entre la \emph{entropía cruzada en \({i}\)} será.

\begin{equation}
\begin{split}
H_{i}(P,Q) = - \lambda(L)_{i}
\end{split}
\end{equation}

\subsection{Expresión matricial derivada log-verosimilitud}
\label{sec:org2c21df5}

Para llegar a un punto óptimo en la modelación del problema es útil
expresar de manera matricial las operaciones.

Instancias observadas, una matrix X que contiene N elementos
diferentes. Cada uno de ellos un \emph{vector} que describe las \emph{d}
características, además de la constante.

\begin{equation}
\begin{split}
\tilde{X} = [\tilde{x_1}^T,...,\tilde{x_N}^T]^T \\
\tilde{x_i} = [x_1,...,x_d,1]^T
\end{split}
\end{equation}

Además, la representación de la salida Y, como una matriz one\_hot en
que cada clase tiene un valor único 1 en su columna específica.

\begin{equation}
\begin{split}
[Y]_{i,j}= Q(C_j|x_i) = 1_{t_i = j}
\end{split}
\end{equation}

Es decir, si se tienen tres clases:

\begin{equation}
\begin{split}
[Y]_{1}= [0,0,1] \\
[Y]_{2}= [0,1,0] \\
[Y]_{3}= [1,0,0] \\
\end{split}
\end{equation}

Entonces, dado un conjunto o vector target, se representa de manera
correspondiente según cada vector one-hot.

Asimismo, para evaluar la probabilidad predicha.

\begin{equation}
\begin{split}
[\hat{Y}]_{i,j}= P(C_j | x_i) \\
\end{split}
\end{equation}

En que la probabilidad predicha está definida por la función \emph{sofmax}
en que se reinterpreta la entrada X, por su operación con W, la matrix
de  parámetros a encontrar. 

Además, considerando un modelo de regresión logística multiclase, la
función \emph{softmax}.

\begin{equation}
\begin{split}
P(C_j | x_i) = \frac {exp(x_i^T w_j)} {\sum_{p=1}^{M} exp(x_i^T w_p)}
\end{split}
\end{equation}

En que cada \(w_j\) son los pesos de cada clase \(C_j\), al que habría que
incluir la constante, determinando una matríz \(W = [w_0,w_1,...,w_M]
\in \mathbb{R}^{d+1 x M}\)  de parámetros y su constante.

Es decir, la predición para \(Y_{i}\), se define al operar cada
característica de \(\tilde{X}_{i}\) (considerando la constante), por la
matriz W.

Siendo el vector de predicciones.

\begin{equation}
\begin{split}
\hat{Y}_i = [softmax(w_1^T x_i),...,softmax(w_M^T x_i),]
\end{split}
\end{equation}


A demostrar que. 

\begin{equation}
\begin{split}
\frac {d \lambda(L)} {dW} = \tilde{X}^T (Y -\hat{Y})
\end{split}
\end{equation}

Se tiene la relación de la log-verosimilitud.

\begin{equation}
\begin{split}
\lambda(L) &=  \sum_{i=1}^{N} \sum_{j=1}^{M}  Q(C_j |x_i) log \{ P(C_j | x_i) \} \\
&=  \sum_{i=1}^{N} \sum_{j=1}^{M}  y_{ij} log \{ P(C_j | x_i) \} \\
&=  \sum_{i=1}^{N} \sum_{j=1}^{M}  y_{ij} log \{ \frac {exp(w_j^T x_i)} {\sum_{p=1}^M {exp(w_p^T x_i)}} \} \\
&=  \sum_{i=1}^{N} \sum_{j=1}^{M}  y_{ij} log \{ \frac {exp(w_j^T x_i)} {\sum_{p=1}^M {exp(w_p^T x_i)}} \} \\
&=  \sum_{i=1}^{N} \{ \sum_{j=1}^{M}  y_{ij} w^T_j x_i \} - log \{ {\sum_{p=1}^M {exp(w_p^T x_i)}} \} \\
\end{split}
\end{equation}

Esta expresión es válida ya que, siendo \(y_{ij}=1\) solo para un j de \{1,\ldots{}M\}:

\begin{equation}
\begin{split}
y_{ij} log \{ {\sum_{p=1}^M {exp(w_p^T x_i)}} \} \\
log \{ {\sum_{p=1}^M {exp(w_p^T x_i)}} \}^{y_{ij}} \\
log \{ {\sum_{p=1}^M {exp(w_p^T x_i)}} \} \\
\end{split}
\end{equation}

Luego, se realiza la operación derivada sobre \(\lambda(L)\), observando
que se tienen dos expresiones (A y B).

Teniendo que l=\{1\ldots{}d+1\} y m=\{1\ldots{}.M\}.

\begin{equation}
\frac {d A} {d w_{lm}} &= x_{il} * y_{lm}  
\end{equation}

La derivada en según W en una característica.

\begin{equation}
\frac {d A} {d w_{l}} &= (x_{i1},...,x_{i,d+1}) * y_{lm} 
\end{equation}

La derivada segun W.

\begin{equation}
\frac {d A} {d w} &= \tilde{X}^T * Y 
\end{equation}


Así como también, la expresión B, que consiste en derivar log(suma exp).

\begin{equation}
\begin{split}
\frac {d B} {d w_{lm}} &= \frac {d \sum_{p=1}^M exp(w_c^T x_i)} {d w_{lm}} \frac {1} {\sum_{p=1}^M exp(w_c^T x_i)} \\ 
&= \frac {d (exp(w_m^T x_i))} {d w_{lm}} \frac {1} {\sum_{p=1}^M exp(w_c^T x_i)} \\
&= x_{il} \hat{y_{im}}  \\
\end{split}
\end{equation}

Según una fila de característica. 

\begin{equation}
\begin{split}
\frac {d B} {d w_{l}} &= (x_i1,..., x_i{d+1}) * \hat{y}_{lm}  
\end{split}
\end{equation}

Según toda W.

\begin{equation}
\begin{split}
\frac {d B} {d w} &= \tilde{X}^T * \hat{Y} 
\end{split}
\end{equation}

Por lo tanto, en conjunto se tendrá.

\begin{equation}
\begin{split}
\frac {d \lambda(L)} {d W} = \tilde{X}^T  (Y - \hat{Y})
\end{split}
\end{equation}

\subsection{Equivalencia log-verosimilitud negativa y entropía cruzada}
\label{sec:org749c8a1}

Si bien la log-verosimilitud habla de un factor de certeza que
aproxima ciertos parámetros a un modelo de un fenómneno, y la entropía
cruzada habla de la relación entre los valores reales y una predicción
basada en un modelo, ambos métodos se relacionan desde dos enfoques
diferentes para llegar al mismo resultado. Esto se observa en la
relación de la ecuación (H = - LL). 

\section{P2. Regresion Log�stica Multiclase}
\label{sec:org44efdf3}


\subsection{Introducción}
\label{sec:org2a97787}

La solución de este problema permitirá entrenar y comparar modelos de
Regresión Logística Multiclase basados en funciones polinomiales de
diferente grado bajo los criterios:

\begin{itemize}
\item información de Akaike
\item información Bayesiano
\item validación cruzada Monte Carlo
\end{itemize}

Se implementa el método descenso del gradiente estocástico (SGD),
utilizando la derivada de la log-verosimilitud.

Considerando el conjunto de observaciones de P1, las estrucuras
matriciales definidas también ahí, la iteración SGD para definir los
parámetros W, se determina de la manera siguiente.

\begin{equation}
W_{k+1} = W_k + \eta \frac{d\lambda(L)} {dW}, k=0,1...
\end{equation}

\subsubsection{Implementación de softmax, one\_hot y clase LogisticRegression.}
\label{sec:org58cf5ce}

En el archivo \emph{logistic\_regression.py} se implementa lo pedido.
Utilizando typing matricial para definir entradas y salidas, se
entrega lo siguiente.

Para \textbf{softmax} se debe entregar un vector de M elementos, cada uno
representando una clase. Aplica a cada elemento la \emph{sofmax}, se
verifica que los valores son correctos en \emph{test\_logistic\_regression.py} van entre 0 y 1.

La función \textbf{label\_to\_onehot} crea una tabla diccionario que relaciona
el valor de cada clase con un vector único que contiene un 1 y el
resto de los elementos es 0. El tamaño de cada vector es equivalente a
la cantidad diferente de clases observadas.

De define una clase \textbf{LogLikehood} que permita registrar los valores de
interés de cada iteración, entre ellos la log-verosimilitud. Además de
contener dos propiedades \emph{aic} y \emph{bic} que nos servirán como
indicadores del modelo.

Al realizar \emph{fit} en la instancia, con los valores X e y, se realiza
una iteración de tipo SGD, en que en cada paso se reordenan las
posiciones de inspección de X. Dentro de cada iteración se hace un
ajuste uno a uno de los valores que contiene X (fila a fila).

La iteración se detiene cuando la pendiente del error de la
log-verosimilitud entre dos pasos consecutivos es suficientemente
pequeña. Es decir, se implementa un control de detención suave, que
considera un buffer de los últimos diez valores de la pendiente, cuya
media sea menor a un valor de detención. 

Además para evitar oscilaciones del algoritmo en convergencia, en casos de valores de
tasa de aprendizaje grandes que se acerquen a la condición de
detención pero no logren llegar debido al tamaño mismo de \emph{eta}, se
regula disminuyendolo en un 10\% cada vez que se cumpla la condición.

Con esto, se entrena en modo de pruebas para diversos \emph{eta} y una
cantidad de iteraciones grande como límite (hasta 6000). Con el
algoritmo resultante la mayoría retorna un resultado satisfactorio
entre las 150 a 250 iteraciones\ref{fig:org1113ecd}. Estas pruebas están programadas en
"test\_logistic\_regression.py".

\begin{figure}[htbp]
\centering
\includegraphics[width=280px]{p2/img/logistic_reg.png}
\caption{\label{fig:org1113ecd}Gráfica de log-verosimilitud y valores relacionados}
\end{figure}

\subsection{Implementación de scores}
\label{sec:org1313507}

Adicionalmente a los valores Akaike y BIC, se implementa un algoritmo
que permita inferir mediante metodología Montecarlo un modelo basado
en la estadística de sus resultados, para cada partición se evalúa
además un puntaje. Esto debe considerar la separación en tramos
aleatorios de los conjunto de entrenamiento y de  prueba, además de hacer múltiples iteraciones diferentes, llevando
registro de ello. 

\begin{figure}[htbp]
\centering
\includegraphics[width=280px]{p2/img/plots_poly.png}
\caption{\label{fig:orga3ae1df}Puntajes BIC, AIC, ACC particiones .5 a .9}
\end{figure}

Se determina una partici�n adecuada de 56\%. Se realiz� una simulacion
de 50\% a 90\%, en la cual los mejores puntajes se encuentran entre 55\%
y 65\%.

Con esto en consideraci�n, se definen 2000 iteraciones para caso. Dado
que cada iteraci�n viene siendo independiente de la otra se implemento
un algoritmo que realiza las particiones y asigna la operacio  en
paralelo, adem�s de una funcion recolectora de los resultados. Luego
de unas 8hr se obtuvo el registro de cada experimento. La ejecuci�n de
esta tarea se realiza principalmente con \emph{read\_abstract\_and\_model.py}
y \emph{scoring.py}.

De la simulaci�n resultante se puede observar en ambos casos
 polinomiales que pr�cticamente cualquier modelo que se escoja podria
 servir ya que la precisi�n se mantiene sobre el 96\%. 

\begin{figure}[htbp]
\centering
\includegraphics[width=280px]{p2/img/sim_montecarlo.png}
\caption{\label{fig:orgb24fbd0}Simulacicon montecarlo 2000 iteraciones}
\end{figure}


\subsection{Evaluar modelos elegidos en test}
\label{sec:org3a941e1}

De la simulacion montecarlo se escoger� aquellos modelos que tengan un
m�ximo \textbf{acc} y de estos le menor de la media aic y bic.

Cargando los datos sobre cada modelo seleccionado (uno por sistema
polinomial). Usando \emph{read\_data\_test.py} sobre la simulaci�n.

\begin{center}
\begin{tabular}{llr}
tipo & medida & valor\\
\hline
lineal & score & .94\\
lineal & aic & -6912.28\\
lineal & bic & -6912.91\\
poly3 & score & .96\\
poly3 & aic & -14341.13\\
poly3 & bic & -14299.07\\
\end{tabular}
\end{center}

En t�rminos pr�cticos, las valoraciones del modelo para aic y bic son
bien similares, se puede considerar buenos ya que mantienen un buen
equilibrio entre certeza y complejidad. El puntaje obtenido es
satisfactorio, levemente inferior a lo obtenido para entrenamiento (un
3\%).


\subsection{Modelo general Montercarlo ponderado.}
\label{sec:org860f1fc}

Se busca encontrar un modelo promedio, extra�do de las ponderaciones
\emph{sofmax} sobre cada valor de las puntuaciones AIC, BIC. Es decir, en
vez de tener una suma uniforme y promediarla por N, se pondera cada
puntaje por su importancia como modelo. En esto \emph{softmax} nos provee
una buena forma de encontrar una constante adecuada.

En el mismo script \emph{read\_model\_test} se construye la funci�n que
tomar� cada puntaje y crear� la ponderaci�n. Considerando la
composici�n de dos sistemas de polinomios y dos formas de valoraci�n,
tendr�mos finalmente cuatro modelos. Esta es \emph{ponderacion}, que
termina guardando los modelos ponderados en
\emph{montecarlo-models-ponderado.pydat}.

Se tienen los siguientes puntajes. Corriendo \emph{read\_data\_test\_ponderado}
con el dataset de prueba.

\begin{center}
\begin{tabular}{lrrr}
tipo & AIC & BIC & LL\\
\hline
lineal & .94 & .935 & 889.6\\
poly3 & .955 & .955 & 1791.15\\
\end{tabular}
\end{center}

Se observa que estos modelos se pueden interpretar como una
combinaci�n entre las ponderaciones BIC, AIC y scoring de correlaci�n
cruzada. Lo que da claridad sobre que estrucura de modelo a escoger,
resultando una polinomial de grado 3 ya que, en cierto sentido\ref{fig:org78f178d}, libera
m�s caracter�sticas que permitan ajustar correctamente el modelo. M�s
all� de eso la diferencia es �nfima de un 1\%.

Se hace el estudio cambiando la ponderaci�n (1/2) en torno a (.4) 
y no cambia el puntaje final. Las variaciones son �nfimas en las
matrices ponderadas. Sin embargo, por 

\begin{figure}[htbp]
\centering
\includegraphics[width=280px]{p2/img/montecarlo-ponderado.png}
\caption{\label{fig:org78f178d}Clasificacion Log�stica Multiclase Ponderada}
\end{figure}

\section{P3. SVM}
\label{sec:org5c99b3c}

\subsection{Introducci�n}
\label{sec:org33632a6}

Se implementa \emph{support vector machine} (SVM) para datasets provistos por sklearn \emph{load\_digits}.

El dataset consiste en entrada x con 64 caracteristicas, como salida
el vector \emph{target} que indica una cifra de \{0,1,2,3,4,5,6,7,8,9\}. 

\subsubsection{Formulaci�n por margen suave}
\label{sec:orgef6a280}

Se reclasifica \emph{target} con -1 para digitos no \textbf{0} y 1 para digito \textbf{0}.
\end{document}