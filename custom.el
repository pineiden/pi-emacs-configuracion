;; activar paqueteria

(require 'package)
(setq package-archives '(("melpa Stable" . "http://stable.melpa.org/packages/")
                         ("gnu" . "http://elpa.gnu.org/packages/")))
(setq package-user-dir (expand-file-name "elpa/" user-emacs-directory))

;; Install straight.el
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(unless package-archive-contents
  (package-refresh-contents))
(package-install-selected-packages)

(defun ensure-package-installed (&rest packages)
  "Assure every package is installed, ask for installation if it’s not.

Return a list of installed packages or nil for every skipped package."
  (mapcar
   (lambda (package)
     ;; (package-installed-p 'evil)
     (if (package-installed-p package)
         nil
       (if (y-or-n-p (format "Package %s is missing. Install it? " package))
           (package-install package)
         package)))
   packages))

;; make sure to have downloaded archive description.
;; Or use package-archive-contents as suggested by Nicolas Dudebout
(or (file-exists-p package-user-dir)
    (package-refresh-contents))

(ensure-package-installed 'iedit 'magit) ;  --> (nil nil) if iedit and magit are already installed


(setq package-list
      '(cape                ; Completion At Point Extensions
        orderless           ; Completion style for matching regexps in any order
        vertico             ; VERTical Interactive COmpletion
        marginalia          ; Enrich existing commands with completion annotations
        consult             ; Consulting completing-read
        corfu               ; Completion Overlay Region FUnction
        deft                ; Quickly browse, filter, and edit plain text notes
        elfeed              ; Emacs Atom/RSS feed reader
        elfeed-org          ; Configure elfeed with one or more org-mode files
        citar               ; Citation-related commands for org, latex, markdown
        citeproc            ; A CSL 1.0.2 Citation Processor
        flyspell-correct-popup ; Correcting words with flyspell via popup interface
        flyspell-popup      ; Correcting words with Flyspell in popup menus
        guess-language      ; Robust automatic language detection
        helpful             ; A better help buffer
        mini-frame          ; Show minibuffer in child frame on read-from-minibuffer
        imenu-list          ; Show imenu entries in a separate buffer
        magit               ; A Git porcelain inside Emacs.
        markdown-mode       ; Major mode for Markdown-formatted text
        multi-term          ; Managing multiple terminal buffers in Emacs.
        pinentry            ; GnuPG Pinentry server implementation
        use-package         ; A configuration macro for simplifying your .emacs
        which-key
	))         ; Display available keybindings in popup

;; Install packages that are not yet installed
(dolist (package package-list)
  (straight-use-package package))


;; Recovvering

(setq auto-save-list-file-prefix ; Prefix for generating auto-save-list-file-name
      (expand-file-name ".auto-save-list/.saves-" user-emacs-directory)
      auto-save-default t        ; Auto-save every buffer that visits a file
      auto-save-timeout 20       ; Number of seconds between auto-save
      auto-save-interval 200)    ; Number of keystrokes between auto-saves

;; Backup

(setq backup-directory-alist       ; File name patterns and backup directory names.
      `(("." . ,(expand-file-name "backups" user-emacs-directory)))
      make-backup-files t          ; Backup of a file the first time it is saved.
      vc-make-backup-files nil     ; No backup of files under version contr
      backup-by-copying t          ; Don't clobber symlinks
      version-control t            ; Version numbers for backup files
      delete-old-versions t        ; Delete excess backup files silently
      kept-old-versions 6          ; Number of old versions to keep
      kept-new-versions 9          ; Number of new versions to keep
      delete-by-moving-to-trash t) ; Delete files to trash

;; recent files

(require 'recentf)

(setq recentf-max-menu-items 50
      recentf-exclude '("/Users/rougier/Documents/Mail.+"))

(let (message-log-max)
  (recentf-mode 1))

;; History 

(defun unpropertize-kill-ring ()
  (setq kill-ring (mapcar 'substring-no-properties kill-ring)))
(add-hook 'kill-emacs-hook 'unpropertize-kill-ring)

(require 'savehist)

(setq kill-ring-max 50
      history-length 50)

(setq savehist-additional-variables
      '(kill-ring
        command-history
        set-variable-value-history
        custom-variable-history   
        query-replace-history     
        read-expression-history   
        minibuffer-history        
        read-char-history         
        face-name-history         
        bookmark-history
        file-name-history))

 (put 'minibuffer-history         'history-length 50)
 (put 'file-name-history          'history-length 50)
 (put 'set-variable-value-history 'history-length 25)
 (put 'custom-variable-history    'history-length 25)
 (put 'query-replace-history      'history-length 25)
 (put 'read-expression-history    'history-length 25)
 (put 'read-char-history          'history-length 25)
 (put 'face-name-history          'history-length 25)
 (put 'bookmark-history           'history-length 25)
(setq history-delete-duplicates t)

(let (message-log-max)
  (savehist-mode))

;; cursor

(setq save-place-file (expand-file-name "saveplace" user-emacs-directory)
      save-place-forget-unreadable-files t)

(save-place-mode 1)

;; Custom

;; (setq custom-file (concat user-emacs-directory "custom.el"))

;; (when (file-exists-p custom-file)
;;   (load custom-file nil t))

;;S erver
(require 'server)

(unless (server-running-p)
  (server-start))




;; update package

(when (version< emacs-version "27")
  (setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3"))

(package-refresh-contents)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package)
  )
(use-package delight :ensure t)
(use-package use-package-ensure-system-package :ensure t)

;; iconos

;; or
(use-package all-the-icons
  :if (display-graphic-p))

;; codificacion


(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(if (boundp 'buffer-file-coding-system)
(setq-default buffer-file-coding-system 'utf-8)
(setq default-buffer-file-coding-system 'utf-8))
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))


;; tablero de inicio

(use-package dashboard
  :preface
    (setq dashboard-banner-logo-title (concat "GNU Emacs " emacs-version
					    " kernel " (car (split-string (shell-command-to-string "uname -r") "-"))
					    " x86_64 " (car (split-string (shell-command-to-string "/usr/bin/sh -c '. /etc/os-release && echo $PRETTY_NAME'") "\n"))))
  :init
    (add-hook 'after-init-hook 'dashboard-refresh-buffer)
  :custom (dashboard-set-file-icons t)
  :custom (dashboard-startup-banner 'logo)
  :config (dashboard-setup-startup-hook))


;;;;;;;;;;;;;;;;,
;;;;;;;;;;;;;;;;;;;;;;;

;; load lsp settings
(message "loading config lsp")
(setq lsp-file (expand-file-name "lsp.org" user-emacs-directory))
 (when (file-exists-p lsp-file)
   (org-babel-load-file lsp-file))


(require 'lsp-mode)
(use-package lsp-mode)
;; (use-package lsp-mode
;;  :init
;;  ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
;;  (setq lsp-keymap-prefix "C-c l")
;;  :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
;; 		(rust-mode . lsp)
;; 		(python-mode . lsp)
;; 		;; if you want which-key integration
;; 		(lsp-mode . lsp-enable-which-key-integration))
;;  :commands lsp)

;; (use-package which-key
;;   :ensure
;;   :init
;;   (which-key-mode))

;; (use-package selectrum
;;   :ensure
;;   :init
;;   (selectrum-mode)
;;   :custom
;;   (completion-styles '(flex substring partial-completion)))

(setq lsp-python-ms-python-executable "python3")
(setq lsp-python-ms-extra-paths "/usr/bin/python3")

;; ;; optionally
;; (use-package lsp-ui :commands lsp-ui-mode)
;; ;; if you are helm user
;; (use-package helm-lsp :commands helm-lsp-workspace-symbol)

;; (use-package company-tabnine :ensure t)
;; (add-to-list 'company-backends #'company-tabnine)

(setq lsp-enable-links nil)


;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;



;; enable flycheck global
(package-install 'flycheck)
(require 'flycheck) 

(use-package flycheck
  :init
  ;; (add-hook 'after-init-hook #'global-flycheck-mode)
  (add-hook 'prog-mode-hook #'flycheck-mode)
  :config
  (setq flycheck-check-syntax-automatically '(save new-line)
	 flycheck-idle-change-delay 5.0
	 flycheck-display-errors-delay 0.9
	 flycheck-highlighting-mode 'symbols
	 flycheck-indication-mode 'left-fringe
	 ;; 'flycheck-fringe-bitmap-double-arrow
	 flycheck-standard-error-navigation t ; [M-g n/p]
	 flycheck-deferred-syntax-check t
	 ;; flycheck-mode-line '(:eval (flycheck-mode-line-status-text))
	 flycheck-completion-system 'ido; 'ido, 'grizzl, nil
	 )
)

;; activar modo global de flycheck-mode
(global-flycheck-mode)
(setq flycheck-flake8rc "~/.emacs.d/checkers/flake8rc.ini")
(add-hook 'after-init-hook #'global-flycheck-mode)
;;(require 'flycheck-mypy)
;;(add-hook 'python-mode-hook 'flycheck-mode)
(flycheck-add-next-checker 'python-flake8 'python-pylint t)
;;(flycheck-add-next-checker 'python-flake8 'python-mypy t)


(use-package hydra
  :defer 2
  :bind ("C-c f" . hydra-flycheck/body))


(defhydra hydra-zoom (global-map "<f2>")
  "zoom"
  ("g" text-scale-increase "in")
  ("l" text-scale-decrease "out"))

(defhydra hydra-buffer-menu (:color pink
                             :hint nil)


  "
^Mark^             ^Unmark^           ^Actions^          ^Search
^^^^^^^^-----------------------------------------------------------------
_m_: mark          _u_: unmark        _x_: execute       _R_: re-isearch
_s_: save          _U_: unmark up     _b_: bury          _I_: isearch
_d_: delete        ^ ^                _g_: refresh       _O_: multi-occur
_D_: delete up     ^ ^                _T_: files only: % -28`Buffer-menu-files-only
_~_: modified
"
  ("m" Buffer-menu-mark)
  ("u" Buffer-menu-unmark)
  ("U" Buffer-menu-backup-unmark)
  ("d" Buffer-menu-delete)
  ("D" Buffer-menu-delete-backwards)
  ("s" Buffer-menu-save)
  ("~" Buffer-menu-not-modified)
  ("x" Buffer-menu-execute)
  ("b" Buffer-menu-bury)
  ("g" revert-buffer)
  ("T" Buffer-menu-toggle-files-only)
  ("O" Buffer-menu-multi-occur :color blue)
  ("I" Buffer-menu-isearch-buffers :color blue)
  ("R" Buffer-menu-isearch-buffers-regexp :color blue)
  ("c" nil "cancel")
  ("v" Buffer-menu-select "select" :color blue)
  ("o" Buffer-menu-other-window "other-window" :color blue)
  ("q" quit-window "quit" :color blue))

(define-key Buffer-menu-mode-map "." 'hydra-buffer-menu/body)
;;;
(setq lsp-keymap-prefix "s-l")

(require 'use-package)
(setq use-package-always-ensure 't)


;; (dolist (package '(use-package))
;;    (unless (package-installed-p company)
;;        (package-install company)))

;; activa paquete
(use-package company
  :init
  (global-company-mode)
  )
;; (add-to-list 'company-backends #'company-tabnine)

;; ;; ;; company-tabnine
;; (setq ct-custom-file (expand-file-name
;; 				   "extras/company-tabnine/company-tabnine.el"
;; 				   user-emacs-directory))
;; (when (file-exists-p ct-custom-file)
;;    (load ct-custom-file))

;; ;; (define-key company-active-map (kbd "C-<f1>") #'my/company-show-doc-buffer)
;; ;;(use-package company-tabnine :ensure t)	;

;; ;;; cosas visuales.

(setq inhibit-startup-message t)
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

;; ;; Theme
(load-theme 'wombat t)
;; ;; con la rueda del mouse y C ->> zoom
;; ;; (global-set-key (kbd "<C-wheel-up>") 'text-scale-increase)
;; ;; (global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)

;; ;; (add-hook 'text-mode-hook 'visual-line-mode)

;; ;; adding slime configuration

;;(load (expand-file-name "~/.quicklisp/slime-helper.el"))
(setq inferior-lisp-program "sbcl")

;; ;; load languages
;; ;; abrir en pantalla completa
(add-hook 'emacs-startup-hook 'toggle-frame-maximized)

;; ;;settings formato texto
;; ;;(load-theme 'leuven t)
(fset 'yes-or-no-p 'y-or-n-p)
(recentf-mode 1)
(setq recentf-max-saved-items 100
      inhibit-startup-message t
      ring-bell-function 'ignore)

(tool-bar-mode 0)
(menu-bar-mode 0)
(when (fboundp 'scroll-bar-mode)
  (scroll-bar-mode 0))

(cond
 ((member "Monaco" (font-family-list))
  (set-face-attribute 'default nil :font "Monaco-12"))
 ((member "Inconsolata" (font-family-list))
  (set-face-attribute 'default nil :font "Inconsolata-12"))
 ((member "Consolas" (font-family-list))
  (set-face-attribute 'default nil :font "Consolas-11"))
 ((member "DejaVu Sans Mono" (font-family-list))
  (set-face-attribute 'default nil :font "DejaVu Sans Mono-10")))

;;;;;;;;;;
(require 'org)
(require 'ob-lisp)
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)

;; ;:::: Activar evaluación de lenguajes en orgmode
;; ;; auto-load
;; (add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
;; (global-set-key "\C-cl" 'org-store-link)
;; (global-set-key "\C-ca" 'org-agenda)


;; ;;(package-initialize)

(message "loading config base")
(setq config-file (expand-file-name "config.org" user-emacs-directory))
(when (file-exists-p config-file)
  (org-babel-load-file config-file))

;; orgmode

(message "loading config base")
(setq orgmode-file (expand-file-name "orgmode.org" user-emacs-directory))
(when (file-exists-p orgmode-file)
  (org-babel-load-file orgmode-file))


;; haskell

(message "loading config base")
(setq haskell-file (expand-file-name "haskell.org" user-emacs-directory))
(when (file-exists-p haskell-file)
  (org-babel-load-file haskell-file))


;; ;; python3 mode
(message "loading config python")
(setq python-file (expand-file-name "python.org" user-emacs-directory))
(when (file-exists-p python-file)
  (org-babel-load-file python-file))

;; enable and configure dap-mode
(message "loading config web mode: html, css, js")
(setq web-file (expand-file-name "web.org" user-emacs-directory))
(when (file-exists-p web-file)
  (org-babel-load-file web-file))

;; enable and configure dap-mode
(message "loading config dap debugger")
(setq dap-file (expand-file-name "dap.org" user-emacs-directory))
(when (file-exists-p dap-file)
  (org-babel-load-file dap-file))

;; enable and configure dap-mode
(message "loading config magit")
(setq git-file (expand-file-name "magit.org" user-emacs-directory))
(when (file-exists-p git-file)
  (org-babel-load-file git-file))

;; enable and configure dap-mode
;; (message "loading config tabnine")
;; (setq git-file (expand-file-name "tabnine.org" user-emacs-directory))
;; (when (file-exists-p git-file)
;;   (org-babel-load-file git-file))

;; ;; enable and configure dap-mode
;; ;; (message "loading config javascript and fr modes")
;; ;; (setq js-file (expand-file-name "js_ts.org" user-emacs-directory))
;; ;; (when (file-exists-p js-file)
;; ;;   (org-babel-load-file js-file))


;; ;; load rustic settings
;; ;; (message "loading config rustic")
(setq rustic_file (expand-file-name "rustic_conf.org" user-emacs-directory))
(when (file-exists-p rustic_file)
  (org-babel-load-file rustic_file))



;; Encoding

(set-default-coding-systems 'utf-8)     ; Default to utf-8 encoding
(prefer-coding-system       'utf-8)     ; Add utf-8 at the front for automatic detection.
(set-default-coding-systems 'utf-8)     ; Set default value of various coding systems
(set-terminal-coding-system 'utf-8)     ; Set coding system of terminal output
(set-keyboard-coding-system 'utf-8)     ; Set coding system for keyboard input on TERMINAL
(set-language-environment "Spanish")    ; Set up multilingual environment


;; ;; definetively tabs -> spaces

;; (setq standard-indent 4)
;; (setq-default indent-tabs-mode nil)
;; (setq tab-width 4) ; or any other preferred value
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("78e9a3e1c519656654044aeb25acb8bec02579508c145b6db158d2cfad87c44e" "fe1c13d75398b1c8fd7fdd1241a55c286b86c3e4ce513c4292d01383de152cb7" default))
 '(package-selected-packages
   '(all-the-icons-dired all-the-icons-ivy-rich treemacs-all-the-icons flycheck zenburn-theme yaml-mode yaml which-key web-mode web-beautify virtualenvwrapper verb use-package-ensure-system-package toml-mode sr-speedbar sql-indent selectrum rustic rainbow-delimiters pylint pyenv-mode-auto plantuml-mode org-web-tools org-projectile ob-http nlinum-relative nlinum-hl neotree magit lsp-ui http helm-lsp haskell-snippets haskell-mode flymake-python-pyflakes flycheck-rust flycheck-pyre flycheck-pyflakes flycheck-mypy flycheck-ini-pyinilint exec-path-from-shell electric-cursor dracula-theme delight dashboard dap-mode all-the-icons)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
