(add-to-list 'auto-mode-alist '("\\.css$" . html-mode))
(add-to-list 'auto-mode-alist '("\\.cfm$" . html-mode))

(add-hook 'html-mode-hook
		  (lambda ()
			(require 'filladapt)
			(set (make-local-variable 'filladapt-token-table)
				 (append filladapt-token-table
						 '(("<li>[ \t]" bullet))))))

(setq sgml-xml-mode t)

(load "~/.emacs.d/extras/web-mode/web-mode.el")

(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.dj.html\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.jinja.html\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.vue\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tsx\\'" . web-mode))

;;(add-to-list 'auto-mode-alist '("\\.api\\'" . web-mode))
;;(add-to-list 'auto-mode-alist '("/some/react/path/.*\\.js[x]?\\'" . web-mode))

;; (setq web-mode-content-types-alist
;;   '(("json" . "/some/path/.*\\.api\\'")
;;     ("xml"  . "/other/path/.*\\.api\\'")
;;     ("jsx"  . "/some/react/path/.*\\.js[x]?\\'")))

(defun my-web-mode-hook ()
  "Hooks for Web mode."
  (setq web-mode-markup-indent-offset 2)
)
(add-hook 'web-mode-hook  'my-web-mode-hook)
(setq web-mode-css-indent-offset 2)
(setq web-mode-code-indent-offset 2)

(setq web-mode-comment-style 2)

;; (set-face-attribute 'web-mode-css-rule-face nil 
;; 					:foreground "Pink3")

(define-key web-mode-map 
  (kbd "C-n") 
  'web-mode-tag-match)

(setq web-mode-enable-current-element-highlight t)
(setq web-mode-enable-current-column-highlight t)

(setq web-mode-style-padding 1)
(setq web-mode-script-padding 1)
(setq web-mode-block-padding 0)
