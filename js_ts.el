;;(add-to-list 'eglot-server-programs '(web-mode "vls"))
(add-to-list 'load-path "~/.emacs.d/extras/vue-mode")
(load "~/.emacs.d/extras/vue-mode/vue-mode.el")
(require 'vue-mode)

(setq vue-mode-packages
	  '(vue-mode))

(setq vue-mode-excluded-packages '())

(defun vue-mode/init-vue-mode ()
  "Initialize my package"
  (use-package vue-mode
	:mode "\\.vue\\'"
	:hook (vue-mode . prettier-js-mode)
	:config
	(add-hook 'vue-mode-hook #'lsp)
	(setq prettier-js-args '("--parser vue")))
  )
