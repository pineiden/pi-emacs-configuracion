;; (use-package company-tabnine :ensure t)
;; (add-to-list 'company-backends #'company-tabnine)

;;(setq lsp-enable-links nil)

(require 'lsp-mode)
;;( use-package lsp-mode
 ;; :init
 ;; ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
 ;; (setq lsp-keymap-prefix "C-c s")
 ;; :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
 ;; 		(rust-mode . lsp)
 ;; 		(python-mode . lsp)
 ;; 		;; if you want which-key integration
 ;; 		(lsp-mode . lsp-enable-which-key-integration))
 ;; :commands lsp)
 
;; (setq lsp-lens-enable t)
;; (setq lsp-headerline-breadcrumb-enable t)
;; (setq lsp-enable-symbol-highlighting t)
;; (setq lsp-ui-doc-enable t)
;; (setq lsp-ui-doc-show-with-cursor t)
;; (setq lsp-ui-doc-show-with-mouse t)
;; (setq lsp-ui-sideline-enable t) 
;; (setq lsp-ui-sideline-show-code-actions t)
;; (setq lsp-ui-sideline-enable t)
;; (setq lsp-ui-sideline-show-hover t)
;; (setq lsp-modeline-code-actions-enable t)
;; (setq lsp-diagnostics-provider :flycheck)
;; (setq lsp-ui-sideline-enable t)
;; (setq lsp-ui-sideline-show-diagnostics t)
;; (setq lsp-eldoc-enable-hover t)
;; (setq lsp-modeline-diagnostics-enable t)
;; (setq lsp-signature-auto-activate t) ;; you could manually request them via `lsp-signature-activate`
;; (setq lsp-signature-render-documentation t)

;; (use-package which-key
;;   :ensure
;;   :init
;;   (which-key-mode))

;; (use-package selectrum
;;   :ensure
;;   :init
;;   (selectrum-mode)
;;   :custom
;;   (completion-styles '(flex substring partial-completion)))

(setq lsp-python-ms-python-executable "python3")
(setq lsp-python-ms-extra-paths "/usr/bin/python3")

;; optionally
;;(use-package lsp-ui :commands lsp-ui-mode)
;; if you are helm user
;;(use-package helm-lsp :commands helm-lsp-workspace-symbol)
