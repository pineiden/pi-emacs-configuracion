#+TITLE: Configuración Python-mode

* Intérprete por defecto



#+begin_src emacs-lisp
(setq python-python-command "/usr/bin/python3")
(setq python-shell-interpreter "/usr/bin/python3")
#+end_src

#+RESULTS:
: /usr/bin/python3



* Projectile



  Configuración para detectar el proyecto completo

  #+begin_src emacs-lisp
    ;; (after-load 'projectile
    ;;   (setq-default
    ;;    projectile-mode-line
    ;;    '(:eval
	;;  (if (file-remote-p default-directory)
	;;      " Pr"
	;;    (format " Pr[%s]" (projectile-project-name))))))
  #+end_src
  

* Company with Jedi




  #+begin_src emacs-lisp
;; (defun company-jedi-setup ()
;;   (add-to-list 'company-backends 'company-jedi))
;; (add-hook 'python-mode-hook 'company-jedi-setup)
  #+end_src


Activación de Jedi para Python


#+begin_src emacs-lisp
;; (setq jedi:setup-keys t)
;; (setq jedi:complete-on-dot t)
;;(add-hook 'python-mode-hook 'jedi:setup)
#+end_src


* Programación Interactiva PTPython




  #+begin_src emacs-lisp
(setq
 python-shell-interpreter "ipython"
 python-shell-interpreter-args "-i")
  #+end_src

  #+RESULTS:
  : -i



* Activación ambiente virtualenvs




#+begin_src emacs-lisp
(require 'virtualenvwrapper)
(venv-initialize-interactive-shells) ;; if you want interactive shell support
(venv-initialize-eshell) ;; if you want eshell support
;; note that setting `venv-location` is not necessary if you
;; use the default location (`~/.virtualenvs`), or if the
;; the environment variable `WORKON_HOME` points to the right place
(setq venv-location "~/.virtualenvs")
#+end_src

#+RESULTS:
: ~/.virtualenvs


A continuación, el uso de 'flycheck' que revisará de manera dinámica el código.



* Enable flycheck with mypy/pylint







Encadenar los /checkers/ de python para flycheck.

#+begin_src emacs-lisp

(defun flycheck-python-setup ()
  (flycheck-mode))
(add-hook 'python-mode-hook #'flycheck-python-setup)

(flycheck-add-next-checker 'python-flake8 'python-pylint 'python-mypy)

(with-eval-after-load 'flycheck
  (add-hook 'flycheck-mode-hook #'flycheck-pycheckers-setup))

(message "Flycheck with python loaded")

;;(setq-default flycheck-indication-mode 'left-margin)
;;(add-hook 'flycheck-mode-hook #'flycheck-set-indication-mode)

;; Adjust margins and fringe widths…
#+end_src

#+RESULTS:
: Flycheck with python loaded





* TEST lSP flycheck 

#+begin_src emacs-lisp

#+end_src
