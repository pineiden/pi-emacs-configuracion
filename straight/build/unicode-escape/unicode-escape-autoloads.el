;;; unicode-escape-autoloads.el --- automatically extracted autoloads  -*- lexical-binding: t -*-
;;
;;; Code:


;;;### (autoloads nil "unicode-escape" "unicode-escape.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from unicode-escape.el

(autoload 'unicode-escape "unicode-escape" nil nil nil)

(autoload 'unicode-escape* "unicode-escape" nil nil nil)

(autoload 'unicode-unescape "unicode-escape" nil nil nil)

(autoload 'unicode-unescape* "unicode-escape" nil nil nil)

(autoload 'unicode-escape-region "unicode-escape" nil nil nil)

(autoload 'unicode-unescape-region "unicode-escape" nil nil nil)

;;;***

(provide 'unicode-escape-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; unicode-escape-autoloads.el ends here
