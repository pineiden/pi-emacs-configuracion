;;; -*- no-byte-compile: t -*-
(define-package "sr-speedbar" "20200616" "Same frame speedbar" 'nil :authors '(("Sebastian Rose" . "sebastian_rose@gmx.de")) :maintainer '("Sebastian Rose" . "sebastian_rose@gmx.de") :url "http://www.emacswiki.org/emacs/download/sr-speedbar.el")
