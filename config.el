(add-hook 'org-mode-hook 'turn-on-auto-fill)

(defun tangle_dir (file)
  (concat (org-entry-get nil "tangle-dir") "/" file)
  )

(setq python-shell-interpreter "/usr/bin/python3")
(setq python-python-command "/usr/bin/python3")
(setq org-babel-python-command "/usr/bin/python3")

(defun org+-flyspell-skip-code (b e _ignored)
  "Returns non-nil if current word is code.
This function is intended for `flyspell-incorrect-hook'."
  (save-excursion
    (goto-char b)
    (memq (org-element-type (org-element-context))
      '(code src-block))))

(defun org+-config-flyspell ()
  "Configure flyspell for org-mode."
  (add-hook 'flyspell-incorrect-hook #'org+-flyspell-skip-code nil t))

(add-hook 'org-mode-hook #'org+-config-flyspell)

;;(setq back_dir "~/.emacs.d/backup")
(defvar backup-dir "~/.emacs.d/backup")
(if (not (file-exists-p backup-dir))
  (make-directory backup-dir)
)
;;(setq backup-directory-alist '(".*"  . backup-dir))

(setq-default tab-width 4)
(setq tab-width 4)
(setq-default tab-always-indent t)

(require 'sr-speedbar)
(setq sr-speedbar-right-side nil)

(use-package all-the-icons)

(add-to-list 'load-path "~/.emacs.d/extras/neotree")
(require 'neotree)
(global-set-key [f8] 'neotree-toggle)

(electric-pair-mode 1)

;; Preset `nlinum-format' for minimum width.
(defun my-nlinum-mode-hook ()
  (when nlinum-mode
    (setq-local nlinum-format
                (concat "%" (number-to-string
                             ;; Guesstimate number of buffer lines.
                             (ceiling (log (max 1 (/ (buffer-size) 80)) 10)))
                        "d"))))
(add-hook 'nlinum-mode-hook #'my-nlinum-mode-hook)

(global-nlinum-mode)

(add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))
(defun my-nov-font-setup ()
  (face-remap-add-relative 'variable-pitch :family "Liberation Serif"
                                           :height 1.0))
(add-hook 'nov-mode-hook 'my-nov-font-setup)

;; (require 'undo-tree)
;; (global-undo-tree-mode)

(require 'rainbow-delimiters)
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)

(require 'yaml-mode)
(add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode))

(use-package vertico :ensure t :config (vertico-mode))

;; Optionally use the `orderless' completion style.
(use-package orderless
  :init
  ;; Configure a custom style dispatcher (see the Consult wiki)
  ;; (setq orderless-style-dispatchers '(+orderless-dispatch)
  ;;       orderless-component-separator #'orderless-escapable-split-on-space)
  (setq completion-styles '(orderless basic)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion)))))

;; Enable richer annotations using the Marginalia package
(use-package marginalia
  ;; Either bind `marginalia-cycle` globally or only in the minibuffer
  :bind (("M-A" . marginalia-cycle)
         :map minibuffer-local-map
         ("M-A" . marginalia-cycle))

  ;; The :init configuration is always executed (Not lazy!)
  :init

  ;; Must be in the :init section of use-package such that the mode gets
  ;; enabled right away. Note that this forces loading the package.
  (marginalia-mode))

(straight-use-package 'selectrum)
(selectrum-mode +1)
;; to make sorting and filtering more intelligent
;;(selectrum-prescient-mode +1)
;; to save your command history on disk, so the sorting gets more
;; intelligent over time
;;(prescient-persist-mode +1)

