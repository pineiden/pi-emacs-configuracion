;; haskell lsp  :: https://github.com/emacs-lsp/lsp-haskell
(add-hook 'haskell-mode-hook #'lsp)
(add-hook 'haskell-literate-mode-hook #'lsp)
