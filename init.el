;; cargamos un archivo de configuración custom

;; (package-initialize)

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file))

(put 'scroll-left 'disabled nil)
