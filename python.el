(setq python-python-command "/usr/bin/python3")
(setq python-shell-interpreter "/usr/bin/python3")

    ;; (after-load 'projectile
    ;;   (setq-default
    ;;    projectile-mode-line
    ;;    '(:eval
	;;  (if (file-remote-p default-directory)
	;;      " Pr"
	;;    (format " Pr[%s]" (projectile-project-name))))))

;; (defun company-jedi-setup ()
;;   (add-to-list 'company-backends 'company-jedi))
;; (add-hook 'python-mode-hook 'company-jedi-setup)

;; (setq jedi:setup-keys t)
;; (setq jedi:complete-on-dot t)
;;(add-hook 'python-mode-hook 'jedi:setup)

(setq
 python-shell-interpreter "ipython"
 python-shell-interpreter-args "-i")

(require 'virtualenvwrapper)
(venv-initialize-interactive-shells) ;; if you want interactive shell support
(venv-initialize-eshell) ;; if you want eshell support
;; note that setting `venv-location` is not necessary if you
;; use the default location (`~/.virtualenvs`), or if the
;; the environment variable `WORKON_HOME` points to the right place
(setq venv-location "~/.virtualenvs")

(defun flycheck-python-setup ()
  (flycheck-mode))
(add-hook 'python-mode-hook #'flycheck-python-setup)

(flycheck-add-next-checker 'python-flake8 'python-pylint 'python-mypy)

;; (with-eval-after-load 'flycheck
;;   (add-hook 'flycheck-mode-hook #'flycheck-pycheckers-setup))

;; (message "Flycheck with python loaded")

;;(setq-default flycheck-indication-mode 'left-margin)
;;(add-hook 'flycheck-mode-hook #'flycheck-set-indication-mode)

;; Adjust margins and fringe widths…


